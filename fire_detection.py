import serial # pip install pyserial
from twilio.rest import Client # pip install twilio

account_sid = 'TWILIO_ACCOUNT_SID'
auth_token = 'TWILIO_AUTH_TOKEN'
messaging_service_sid = 'TWILIO_MESSAGING_SERVICE_SID'
client = Client(account_sid, auth_token)


def fire_detected(img_url):
	serialcomm = serial.Serial('COM3', 9600)
	serialcomm.timeout = 1
	serialcomm.write('red'.encode())
	serialcomm.close()

	client.messages.create(messaging_service_sid=messaging_service_sid, body=f'FIRE ALARAM! {img_url}', to='+213541913941')


def fire_stopped():
	serialcomm = serial.Serial('COM3', 9600)
	serialcomm.timeout = 1
	serialcomm.write('green'.encode())
	serialcomm.close()
