String incomingByte;

#define LED_RED 4
#define LED_GREEN 2

#define MOTOR_A 11
#define MOTOR_B 10

void setup() {
	Serial.begin(9600);
	pinMode(LED_RED, OUTPUT);
	pinMode(LED_GREEN, OUTPUT);
}

void loop() {
	if (Serial.available() > 0) {

		incomingByte = Serial.readStringUntil('\n');

		if (incomingByte == "red") {
			digitalWrite(LED_RED, HIGH);
			digitalWrite(LED_GREEN, LOW);

			digitalWrite(MOTOR_A, HIGH);
			digitalWrite(MOTOR_B, LOW);

			Serial.write("Red led on");
		} else if (incomingByte == "green") {
			digitalWrite(LED_RED, LOW);
			digitalWrite(LED_GREEN, HIGH);

			digitalWrite(MOTOR_A, LOW);
			digitalWrite(MOTOR_B, LOW);

			Serial.write("Green led on");
		} else {

			Serial.write("invald input");
		}
	}
}