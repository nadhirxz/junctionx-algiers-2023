# Flarm - JunctionX Algiers 2023

`fire_detection.ino`: Arduino code to light the LED and activate/deactivate the water pump

`fire_detection.py`: Python code to communicate with the Arduino and send SMS

## Code for AI
[First One - Fire Detection](https://github.com/Dreamoonc/Fire-detection)

[Second One - Fire and Smoke Detection](https://colab.research.google.com/drive/18VKghexUsgO3bNSuu-QsdyWaV3NtOZ68?usp=sharing)
